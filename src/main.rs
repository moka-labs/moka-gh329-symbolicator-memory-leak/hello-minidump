use std::fs::File;

use anyhow::Context;
use clap::{Parser, ValueEnum};
use crash_context::CrashContext;
use crash_handler as ch;
use minidump_writer::minidump_writer::MinidumpWriter;

#[derive(Parser, Debug)]
#[command(author, version, about)]
struct Args {
    flavor: Flavor,
}

#[derive(ValueEnum, Clone, Copy, Debug)]
enum Flavor {
    /// Abort (SIGABRT)
    Abort,
    /// Bus error (SIGBUS)
    Bus,
    /// Illegal instruction (SIGILL)
    Ill,
    /// Segmentation fault (SIGSEGV)
    Segfault,
}

fn main() -> anyhow::Result<()> {
    let program_id = include_str!("program_id.in");
    let program_id = program_id
        .trim()
        .parse::<u32>()
        .context("Failed to parse PROGRAM_ID")?;

    let cli = Args::parse();
    let flavor = cli.flavor;

    let flavor_name = match flavor {
        Flavor::Abort => "abort",
        Flavor::Bus => "bus",
        Flavor::Ill => "illegal-inst",
        Flavor::Segfault => "segfault",
    };

    // Attach a crash handler, which will write a Minidump when this process
    // crashes. Ensure to bind a variable to the handler, otherwise it will be
    // dropped and detached immediately.
    let _handler = ch::CrashHandler::attach(unsafe {
        ch::make_crash_event(move |cc: &ch::CrashContext| {
            let handled = match write_minidump(program_id, flavor_name, cc) {
                Ok(()) => true,
                Err(e) => {
                    eprint!("Failed to write Minidump: {}", e);
                    false
                }
            };
            ch::CrashEventResult::Handled(handled)
        })
    })?;

    // Let's crash this process!
    match flavor {
        Flavor::Abort => unsafe { sadness_generator::raise_abort() },
        Flavor::Bus => unsafe { sadness_generator::raise_bus() },
        Flavor::Ill => unsafe { sadness_generator::raise_illegal_instruction() },
        Flavor::Segfault => unsafe { sadness_generator::raise_segfault() },
    }
}

#[cfg(target_os = "macos")]
fn write_minidump(program_id: u32, flavor_name: &str, cc: &ch::CrashContext) -> anyhow::Result<()> {
    // CrashContext does not implement Clone by some reason, so we need
    // to copy the fields manually(?).
    let crash_context = CrashContext {
        task: cc.task,
        thread: cc.thread,
        handler_thread: cc.handler_thread,
        exception: cc.exception,
    };

    let path = format!(
        "./dumps/hello-minidump-{:05}-{}.mdmp",
        program_id, flavor_name
    );

    // Write a minidump with the crash context.
    let mut writer = MinidumpWriter::with_crash_context(crash_context);
    let mut dump_file = File::create(path).with_context(|| "Failed to create file")?;
    writer.dump(&mut dump_file)?;

    Ok(())
}

#[cfg(target_os = "linux")]
fn write_minidump(cc: &ch::CrashContext) -> anyhow::Result<()> {
    unimplemented!()
}
