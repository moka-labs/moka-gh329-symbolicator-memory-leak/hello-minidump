# Hello Minidump

This program generates a Minidump file that can be used for testing Sentry
Symbolicator with [its stress test tool][stress-tool].

It is related to the following GitHub issue:

- [Memory leak in moka 0.12 #329][moka-gh329]

[stress-tool]: https://github.com/getsentry/symbolicator/tree/master/crates/symbolicator-stress
[moka-gh329]: https://github.com/moka-rs/moka/issues/329

## Supported Platforms

Currently this program only compiles for macOS targets. It should be easy to
support Linux and Windows MSVC though.

I am using macOS Ventura 13.6 (arm64) with Rust 1.72.1.

## Usage

### Installing `symsorter`

Install `symsorter` from Sentry Symbolicator:

```console
$ GIT_URL=https://github.com/getsentry/symbolicator.git
$ GIT_REV=519f4ddf1e

$ cargo install -f --git $GIT_URL --rev $GIT_REV symsorter
```

### Build `hello-minidump` and Sort the Debug Information Files

Edit the five digit positive integer in `hello-minidump/src/program_id.in`, so
that your Mach-O executable should have a unique code ID. The code ID is used by
Symbolicator to identify the debug information files in the symbol servers.

And then, in the `hello-minidump` directory, run the following commands:

```console
$ cargo build --release

## On macOS, you need to generate a dSYM bundle.
$ dsymutil ./target/release/hello-minidump

## Sort the debug information files using Symbolicator's "unified"
## directory structure.
$ symsorter -zz -o symbols --prefix macos --bundle-id 0.1.0 \
     ./target/release/hello-minidump{,.dSYM}

Sorting debug information files
hello-minidump (dbg, arm64) -> symbols/macos/77/b3579631613f8688f637e35e10a643/debuginfo
hello-minidump (exe, arm64) -> symbols/macos/77/b3579631613f8688f637e35e10a643/executable
Writing bundle meta data

Done.
Sorted 2 debug files
```

You may want to repeat the above steps with different digits in `program_id.in`
file to generate multiple debug symbol files. Once finished, upload the contents
of `symbols` directory to an Amazon S3 bucket. (So it will be like
`my-bucket/macos/77/...` in above example)

### Run `hello-minidump` to Generate a Minidump File

Run `hellow-minidump` with one of the following arguments:

- `segfault` &mdash; segmentation fault
- `abort` &mdash; abort
- `bus` &mdash; bus error
- `ill` &mdash; illegal instruction

```console
$ ./target/release/hello-minidump segfault
fish: Job 1, './target/release/hello-minidump…' terminated by
signal SIGSEGV (Address boundary error)

$ ls dumps/
hello-minidump-00001-segfault.mdmp
```

Optionally, you can use `minidump-stackwalk` to get a stack trace:

```console
## Install minidump-stackwalk from Rust Minidump project.
$ cargo install minidump-stackwalk

## Install dump_syms from Mozilla.
$ cargo install dump_syms

## Generate a debug symbol file in Breakpad format.
$ dump_syms -o hello-minidump.sym ./target/release/hello-minidump

## Get the stack trace.
$ minidump-stackwalk --symbols-path ./target/release/hello-minidump.sym \
    hello-minidump-00001-segfault.mdmp

Operating system: Mac OS X
                  13.6.0 22G120
CPU: arm64
     8 CPUs

Crash reason:  EXC_BAD_ACCESS / KERN_INVALID_ADDRESS
Crash address: 0x0000000100000041
Process uptime: 2 seconds

Thread 0 main (crashed)
 0  hello-minidump + 0x49b8
      x0 = 0x0000000000000003     x1 = 0x0000000000000203
      x2 = 0x000000016ba57000     x3 = 0x000000016ba57000
        ...
     x28 = 0x0000000000000000     fp = 0x000000016b84e950
      lr = 0x00000001045b311c     sp = 0x000000016b84e950
      pc = 0x00000001045b49b8
    Found by: given as instruction pointer in context
 1  hello-minidump + 0x3118
      sp = 0x000000016b84e960     pc = 0x00000001045b311c
    Found by: previous frame's frame pointer
 2  hello-minidump + 0x41ec
      sp = 0x000000016b84e990     pc = 0x00000001045b41f0
    Found by: previous frame's frame pointer
 ...

Loaded modules:
0x1045b0000 - 0x104617fff  hello-minidump  ???  (main)
0x191574000 - 0x1915b9edf  libobjc.A.dylib  0.228.0.0
0x1915ba000 - 0x191648587  dyld  ???
...

Unloaded modules:
```

## Memo

Run MinIO using Docker:

```console
$ MINIO_DATA=/path/to/minio/data-directory

$ docker run \
   -p 9000:9000 \
   -p 9090:9090 \
   --user $(id -u):$(id -g) \
   --name minio1 \
   -e "MINIO_ROOT_USER=symbolicator" \
   -e "MINIO_ROOT_PASSWORD=CHANGEME123" \
   -v $MINIO_DATA:/data \
   quay.io/minio/minio server /data --console-address ":9090"
```
